package ru.pcs.online_library.controller;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import ru.pcs.online_library.model.User;
import ru.pcs.online_library.model.dto.UserDto;
import ru.pcs.online_library.service.UserServiceImpl;

import javax.validation.Valid;

@Controller
public class ProfileController {

    private final UserServiceImpl service;

    public ProfileController(UserServiceImpl service) {
        this.service = service;
    }

    @GetMapping("/profile")
    public String getProfile(Model model, @AuthenticationPrincipal User user) {
        model.addAttribute("email", user.getUsername());
        model.addAttribute("name", user.getName());
        return "profile";
    }

    @PostMapping("/profile")
    public String updateProfile(@Valid UserDto userDto, @AuthenticationPrincipal User user) {
        service.update(userDto, user.getId());
        return "redirect:/favorites";
    }
}
