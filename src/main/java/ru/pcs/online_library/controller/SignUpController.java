package ru.pcs.online_library.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.pcs.online_library.model.dto.UserDto;
import ru.pcs.online_library.service.UserServiceImpl;

import javax.validation.Valid;

@RequiredArgsConstructor
@Controller
@RequestMapping("/registration")
public class SignUpController {

    private final UserServiceImpl userService;

    @GetMapping
    public String getSignUpPage() {
        return "sign-up";
    }

    @PostMapping
    public String signUpUser(@Valid UserDto userDto) {
        userService.signUpUser(userDto);
        return "redirect:/login";
    }
}
