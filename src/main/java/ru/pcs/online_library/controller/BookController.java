package ru.pcs.online_library.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.pcs.online_library.model.Book;
import ru.pcs.online_library.model.dto.BookDto;
import ru.pcs.online_library.service.BookServiceImpl;
import ru.pcs.online_library.util.SecurityUtil;

import java.util.List;

@Controller
public class BookController {

    private final BookServiceImpl service;

    public BookController(BookServiceImpl service) {
        this.service = service;
    }

    @GetMapping()
    public String getAllBooks(Model model) {
        List<BookDto> books = service.getAll(SecurityUtil.authUserId());
        model.addAttribute("books", books);
        return "book-catalog";
    }

    @GetMapping("/catalog/add")
    public String addForm(Book book, Model model) {
        model.addAttribute("book", book);
        return "book-add";
    }

    @PostMapping("/catalog/add")
    public String add(@ModelAttribute Book book,
                      @ModelAttribute("file") MultipartFile file) {
        service.add(book, file);
        return "redirect:/";
    }

    @GetMapping("/catalog/{bookId}")
    public String updateForm(@PathVariable Long bookId, Model model) {
        Book book = service.get(bookId);
        model.addAttribute("book", book);
        return "book-update";
    }

    @PostMapping("/catalog/{bookId}/update")
    public String update(@PathVariable Long bookId,
                         @ModelAttribute Book book,
                         @ModelAttribute("file") MultipartFile file) {
        service.update(book, bookId, file);
        return "redirect:/";
    }

    @PostMapping("catalog/{bookId}/delete")
    public String delete(@PathVariable("bookId") Long bookId) {
        service.delete(bookId);
        return "redirect:/";
    }

    @PostMapping("/favorites")
    public String addBookToFavorites(@RequestParam("bookId") Long bookId) {
        service.addBookToFavorites(SecurityUtil.authUserId(), bookId);
        return "redirect:/";
    }

    @PostMapping("favorites/delete")
    public String removeFromFavorites(@RequestParam("bookId") Long bookId) {
        service.removeFromFavorites(SecurityUtil.authUserId(), bookId);

        return "redirect:/favorites";
    }

    @GetMapping("/favorites")
    public String getFavorites(ModelMap model) {
        List<Book> favorites = service.findAllFavorites(SecurityUtil.authUserId());
        model.addAttribute("books", favorites);
        return "favorites";
    }
}
