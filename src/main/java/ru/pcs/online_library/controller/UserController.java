package ru.pcs.online_library.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.pcs.online_library.model.User;
import ru.pcs.online_library.model.dto.UserDto;
import ru.pcs.online_library.service.UserServiceImpl;

@Controller
public class UserController {

    private final UserServiceImpl service;

    public UserController(UserServiceImpl service) {
        this.service = service;
    }

    @GetMapping("/users")
    public String getAllUsers(Model model) {
        model.addAttribute("users", service.findAll());
        return "user-list";
    }

    @GetMapping("/users/{userId}")
    public String getEditPage(@PathVariable Long userId, Model model) {
        User user = service.get(userId);
        model.addAttribute("user", user);
        return "user-edit";
    }

    @PostMapping("/users/{userId}/update")
    public String update(@PathVariable("userId") Long userId,
                         @ModelAttribute UserDto userDto) {
        service.update(userDto, userId);
        return "redirect:/users";
    }

    @PostMapping("/users/{userId}/delete")
    public String delete(@PathVariable Long userId) {
        service.delete(userId);
        return "redirect:/users";
    }
}
