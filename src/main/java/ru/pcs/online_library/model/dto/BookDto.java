package ru.pcs.online_library.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class BookDto {

    private Long id;
    private String name;
    private String author;
    private String description;
    private String imgFileName;
    private boolean favorite;
}