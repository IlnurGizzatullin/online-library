package ru.pcs.online_library.model;

import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name = "book")
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false)
    @NotBlank
    private String name;

    @Column(name = "author", nullable = false)
    @NotBlank
    private String author;

    @Column(name = "description", nullable = false)
    @NotBlank
    private String description;

    @Column(name = "img_file_name", nullable = false)
    @NotBlank
    private String imgFileName;

    @ManyToMany(mappedBy = "favorites")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Set<User> users;
}
