package ru.pcs.online_library.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import ru.pcs.online_library.model.Role;
import ru.pcs.online_library.model.User;
import ru.pcs.online_library.model.dto.UserDto;
import ru.pcs.online_library.repository.UserRepository;

import java.util.Collections;
import java.util.Date;
import java.util.List;

@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;


    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException("User not found"));
        return user;
    }

    @Override
    public void signUpUser(UserDto userDto) {
        User user = User.builder()
                .name(userDto.getName())
                .email(userDto.getEmail())
                .registered(new Date())
                .roles(Collections.singleton(Role.USER))
                .password(passwordEncoder.encode(userDto.getPassword()))
                .build();
        userRepository.save(user);
    }

    public User save(User user) {
        return userRepository.save(user);
    }

    public void update(UserDto userDto, Long userId) {
        User user = userRepository.getById(userId);
        String password = userDto.getPassword();
        user.setName(userDto.getName());
        user.setPassword(StringUtils.hasText(password) ? passwordEncoder.encode(password) : password);
        user.setEmail(userDto.getEmail().toLowerCase());
        save(user);
    }

    public void delete(long id) {
        userRepository.deleteById(id);
    }

    public User get(long id) {
        return userRepository.findById(id).orElse(null);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }
}
