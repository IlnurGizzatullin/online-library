package ru.pcs.online_library.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.pcs.online_library.model.Book;
import ru.pcs.online_library.model.User;
import ru.pcs.online_library.model.dto.BookDto;
import ru.pcs.online_library.repository.BookRepository;
import ru.pcs.online_library.repository.UserRepository;
import ru.pcs.online_library.util.BookUtil;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Service
public class BookServiceImpl implements BookService {

    @Value("${upload.path}")
    private String uploadPath;

    private final BookRepository bookRepository;
    private final UserRepository userRepository;

    public BookServiceImpl(BookRepository repository, UserRepository userRepository) {
        this.bookRepository = repository;
        this.userRepository = userRepository;
    }

    public Book add(Book book, MultipartFile file) {
        saveFile(book, file);
        return bookRepository.save(book);
    }

    public void delete(long id) {
        deleteFile(bookRepository.getById(id));
        bookRepository.deleteById(id);
    }

    public Book get(long id) {
        return bookRepository.findById(id).orElse(null);
    }

    public List<BookDto> getAll(long userId) {
        Set<Book> favorites = Set.copyOf(bookRepository.findAllByUsers_Id(userId));
        return BookUtil.getBookDtos(bookRepository.findAll(), favorites);
    }

    public void update(Book book, Long bookId, MultipartFile file) {
        Book newBook = bookRepository.getById(bookId);
        newBook.setName(book.getName());
        newBook.setAuthor(book.getAuthor());
        newBook.setDescription(book.getDescription());
        saveFile(newBook, file);
        bookRepository.save(newBook);
    }

    public void addBookToFavorites(Long userId, Long bookId) {
        User user = userRepository.getById(userId);
        Book book = bookRepository.getById(bookId);
        user.getFavorites().add(book);
        userRepository.save(user);
    }

    public void removeFromFavorites(Long userId, Long bookId) {
        User user = userRepository.getById(userId);
        Book book = bookRepository.getById(bookId);
        user.getFavorites().remove(book);
        userRepository.save(user);
    }

    public List<Book> findAllFavorites(Long userId) {
        return bookRepository.findAllByUsers_Id(userId);
    }

    private void saveFile(Book book, MultipartFile file) {
        String uuidFile = UUID.randomUUID().toString();
        String resultFilename = uuidFile + "." + file.getOriginalFilename();
        try {
            Files.copy(file.getInputStream(), Path.of(uploadPath + "/" + resultFilename));
        } catch (IOException e) {
            throw new IllegalArgumentException("Can't save the file");
        }
        book.setImgFileName(resultFilename);
    }

    private void deleteFile(Book book) {
        try {
            Files.deleteIfExists(Path.of(uploadPath + "/" + book.getImgFileName()));
        } catch (IOException e) {
            throw new IllegalArgumentException("Can't delete the file");
        }
    }
}