package ru.pcs.online_library.service;

import org.springframework.security.core.userdetails.UserDetailsService;
import ru.pcs.online_library.model.User;
import ru.pcs.online_library.model.dto.UserDto;

import java.util.List;

public interface UserService extends UserDetailsService {

    void signUpUser(UserDto userDto);

    User save(User user);

    void update(UserDto userDto, Long userId);

    void delete(long id);

    User get(long id);

    List<User> findAll();
}
