package ru.pcs.online_library.service;

import org.springframework.web.multipart.MultipartFile;
import ru.pcs.online_library.model.Book;
import ru.pcs.online_library.model.dto.BookDto;

import java.util.List;

public interface BookService {

    Book add(Book book, MultipartFile file);

    void delete(long id);

    Book get(long id);

    void update(Book book, Long bookId, MultipartFile file);

    List<BookDto> getAll(long userId);

    void addBookToFavorites(Long userId, Long bookId);

    void removeFromFavorites(Long userId, Long bookId);

    List<Book> findAllFavorites(Long userId);
}

