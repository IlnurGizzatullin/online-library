package ru.pcs.online_library.util;

import ru.pcs.online_library.model.Book;
import ru.pcs.online_library.model.dto.BookDto;

import java.util.List;
import java.util.Set;

public class BookUtil {

    public static List<BookDto> getBookDtos(List<Book> fromDb, Set<Book> favorites) {
        return fromDb.stream()
                .map(book -> createBookDto(book, favorites.contains(book)))
                .toList();
    }

    public static List<BookDto> getFavoritesDtos(List<Book> fromDb) {
        return fromDb.stream()
                .map(book -> createBookDto(book, true))
                .toList();
    }

    public static BookDto createBookDto(Book book, boolean isInFavorites) {
        return BookDto.builder()
                .id(book.getId())
                .name(book.getName())
                .author(book.getAuthor())
                .description(book.getDescription())
                .imgFileName(book.getImgFileName())
                .favorite(isInFavorites)
                .build();
    }
}
